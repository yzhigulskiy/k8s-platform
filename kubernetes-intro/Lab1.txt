# https://kubernetes.io/ru/docs/tutorials/hello-minikube/
#

Install minikube:
 # curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 \
     && chmod +x minikube
 # sudo mkdir -p /usr/local/bin/
 # sudo install minikube /usr/local/bin/
 # /usr/local/bin/minikube start --vm-driver=virtualbox

Install kubectl:
 # curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
 # mv kubectl /usr/bin/

Install Dashboard:
 # kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-rc6/aio/deploy/recommended.yaml
 # kubectl proxy
 Browser:
 http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/.
 # kubectl apply -f HW_1/user.yaml
 # kubectl apply -f HW_1/role-binding.yaml
 # kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep admin | awk '{print $1}')

Build image:
 # sudo docker build -t yzhigulskiy/web-server:latest HW_1/docker/

Push to Docker Hub:
 # sudo docker login --username=yzhigulskiy docker.io
 # docker push yzhigulskiy/web-server:latest

Create k8s/web-pod.yaml

Apply configuration:
 # kubectl apply -f HW_1/k8s/web-pod.yaml

Proxy port-forward:
 # kubectl port-forward --address 0.0.0.0 nginx 8080:80 -n prod

ReplicaSet:
 # kubectl apply -f HW_1/k8s/web-replicaset.yaml

Job:
 #